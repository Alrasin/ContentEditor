# TODO List

* EmoteList.cshtml - Finish the properties.  Should be copy/pasta work.
* All views used by Weenie edit - displayed col divs need the "vcenter" class.  See IntPropertyList.cshtml as an example.
* Models\Enums folder - All enum values should have [Display(Name = "{val} - {Pretty Printed Name}")] decorations.
** BoolPropertyId
** DidPropertyId
** DoublePropertyId
** IidPropertyId
** IntPropertyId
** SkillId - needs Ids added
** SpellId
* Content pages
* Weenie Search pop-up div for Search screens and places where a weenie id is entered.
* IntPropertyList.cshtml - additional properties that are really enums need to use @Html.EnumDropDownListFor
* EmoteList.cshtml - Emote properties that are really enums need to use @Html.EnumDropDownListFor
* EmoteSetList.cshtml - Finish the properties.  Should be copy/pasta work.
* Weenie Editor - Generators
* Weenie Editor - Contracts
