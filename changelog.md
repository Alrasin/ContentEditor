# DerethForever.ContentEditor Change Log - March 2018

### 2018-04-07
**Behemoth**

* Added validation for duplicate properties
* Removed properties with null values as part of Weenie saving.  Null values caused Phat Json exporting to fail, and is useless data anyway.

### 2018-04-06
**Gold Golem**

* Changed the Sandbox Provider to send GDL / Phat Json when not logged in, on the recent changes listing page.
* Added a fix for weenie position types always saving as invalid. Changed the `PositionList.html` file to includes the position type with the `HiddenFor` method.

**Behemoth**

* Downloading DF Json now gives you DF Json instead of PhatAC Json
* Removing Skills actually removes them now

### 2018-04-05
**Behemoth**

* Added Discord notifications for Weenie Submissions, Weenie Approvals, and World Releases
